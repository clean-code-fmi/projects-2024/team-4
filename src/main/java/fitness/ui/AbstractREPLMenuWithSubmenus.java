package fitness.ui;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public abstract class AbstractREPLMenuWithSubmenus
        extends AbstractREPLMenu {
    private final ArrayList<SubmenuSelectionOption> submenuOptions;

    protected AbstractREPLMenuWithSubmenus(
            final InputStream input,
            final PrintStream output,
            final List<SubmenuSelectionOption> submenuOptions) {
        super(input, output);

        assert submenuOptions != null;
        this.submenuOptions = new ArrayList<>(submenuOptions);
    }

    @Override
    public final void process() {
        super.process();
        processMenuSpecificInitFunctionality();
        processSubmenus();
    }

    protected abstract void processMenuSpecificInitFunctionality();
    protected abstract void processMenuSpecificFunctionality();

    private void processSubmenus() {
        do {
            printSubmenuOptions();

            int submenuChoice = readIntegerFromInput("Select an option: ");

            if (submenuChoice == submenuOptions.size() + 1) {
                return;
            }

            if (submenuChoice >= 1 && submenuChoice <= submenuOptions.size()) {
                SubmenuSelectionOption selectedOption = submenuOptions.get(submenuChoice - 1);
                Menu submenu = selectedOption.submenuSupplier.get();
                submenu.process();
            }

            processMenuSpecificFunctionality();
        }
        while (true);
    }

    private void printSubmenuOptions() {
        for (int i = 0; i < submenuOptions.size(); ++i) {
            SubmenuSelectionOption submenuOption = submenuOptions.get(i);
            printLine(String.format(
                "%d -> %s", i + 1, submenuOption.description));
        }

        printLine(String.format("%d -> Close", submenuOptions.size() + 1));
    }

    public record SubmenuSelectionOption(
            String description,
            Supplier<Menu> submenuSupplier) {
        public SubmenuSelectionOption {
            assert description != null;
            assert submenuSupplier != null;
        }
    }
}
