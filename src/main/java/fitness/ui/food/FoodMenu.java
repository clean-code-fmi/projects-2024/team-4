package fitness.ui.food;

import fitness.ui.AbstractREPLMenuWithSubmenus;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public final class FoodMenu extends AbstractREPLMenuWithSubmenus {
    private FoodMenu(
            final InputStream input,
            final PrintStream output,
            final List<SubmenuSelectionOption> submenuOptions) {
        super(input, output, submenuOptions);
    }

    public static FoodMenu create(
            final InputStream input,
            final PrintStream output) {
        return new FoodMenu(input, output, new ArrayList<>());
    }

    @Override
    protected String name() {
        return "Food";
    }

    @Override
    protected void processMenuSpecificInitFunctionality() {
        printLine("You should see submenus here");
    }

    @Override
    protected void processMenuSpecificFunctionality() {
        printLine("You should see submenus here");
    }
}
