package fitness.ui;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.function.Supplier;

public final class MainMenu extends AbstractREPLMenuWithSubmenus {
    private MainMenu(
            final InputStream input,
            final PrintStream output,
            final List<SubmenuSelectionOption> submenuOptions) {
        super(input, output, submenuOptions);
    }

    public static MainMenu create(
            final InputStream input,
            final PrintStream output,
            final Supplier<Menu> profileMenuSupplier,
            final Supplier<Menu> foodMenuSupplier,
            final Supplier<Menu> waterMenuSupplier,
            final Supplier<Menu> exerciseMenuSupplier) {
        assert profileMenuSupplier != null;
        assert foodMenuSupplier != null;
        assert waterMenuSupplier != null;
        assert exerciseMenuSupplier != null;

        var submenus = List.of(
            new SubmenuSelectionOption("Profile", profileMenuSupplier),
            new SubmenuSelectionOption("Food", foodMenuSupplier),
            new SubmenuSelectionOption("Water", waterMenuSupplier),
            new SubmenuSelectionOption("Exercise", exerciseMenuSupplier));

        return new MainMenu(input, output, submenus);
    }

    @Override
    protected String name() {
        return "Main Menu";
    }


    @Override
    protected void processMenuSpecificInitFunctionality() {
    }

    @Override
    protected void processMenuSpecificFunctionality() {
    }
}
