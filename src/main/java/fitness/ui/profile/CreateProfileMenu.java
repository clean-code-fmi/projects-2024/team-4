package fitness.ui.profile;

import fitness.Profile;
import fitness.ui.AbstractREPLMenu;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Optional;

public final class CreateProfileMenu extends AbstractREPLMenu {

    private Profile userProfile;
    public CreateProfileMenu(
            final InputStream input,
            final PrintStream output) {
        super(input, output);
    }

    public Profile getUserProfile() {
        return userProfile;
    }

    @Override
    protected String name() {
        return "Create Profile";
    }

    @Override
    public void process() {
        super.process();

        int age = enterValidAge();
        int weight = enterValidWeight();
        int height = enterValidHeight();

        userProfile = new Profile(age, weight, height);
    }

    private int enterValidAge() {
        String prompt = "Please enter your age  (must be between "
                + Profile.MIN_AGE + " and " + Profile.MAX_AGE + "): ";

        return readIntegerWithinRange(prompt,
                Optional.of(Profile.MIN_AGE), Optional.of(Profile.MAX_AGE));
    }

    private int enterValidWeight() {
        String prompt = "Please enter your weight (must be positive): ";

        return readIntegerWithinRange(prompt,
                Optional.of(1), Optional.empty());
    }

    private int enterValidHeight() {
        String prompt = "Please enter your height (must be positive): ";

        return readIntegerWithinRange(prompt,
                Optional.of(1), Optional.empty());
    }

}
