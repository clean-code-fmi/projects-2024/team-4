package fitness.ui.profile;

import fitness.Profile;
import fitness.ui.AbstractREPLMenuWithSubmenus;
import fitness.ui.Menu;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.function.Supplier;

public final class ProfileMenu extends AbstractREPLMenuWithSubmenus {

    private final Profile userProfile;

    private ProfileMenu(
            final InputStream input,
            final PrintStream output,
            final List<SubmenuSelectionOption> submenuOptions,
            final Profile userProfile) {
        super(input, output, submenuOptions);
        this.userProfile = userProfile;
    }

    public static ProfileMenu create(
            final InputStream input,
            final PrintStream output,
            final Supplier<Menu> editProfileMenuSupplier,
            final Supplier<Menu> editGoalsMenuSupplier,
            final Profile userProfile) {
        assert editProfileMenuSupplier != null;
        assert editGoalsMenuSupplier != null;

        var submenus = List.of(
            new SubmenuSelectionOption("Edit (personal) data", editProfileMenuSupplier),
            new SubmenuSelectionOption("Edit Goals", editGoalsMenuSupplier));

        return new ProfileMenu(input, output, submenus, userProfile);
    }

    @Override
    protected String name() {
        return "Profile";
    }

    @Override
    protected void processMenuSpecificInitFunctionality() {
        printLine(userProfile.toString());
    }

    @Override
    protected void processMenuSpecificFunctionality() {
        printLine(userProfile.toString());
    }
}
