package fitness.ui.profile.edit.goals;

import fitness.Goal;
import fitness.ui.AbstractREPLMenu;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;

public abstract class AbstractEditGoalsSubmenu extends AbstractREPLMenu {
    public AbstractEditGoalsSubmenu(
            final InputStream input,
            final PrintStream output) {
        super(input, output);
    }


    @Override
    public final void process() {
        super.process();

        editGoals();
    }

    protected abstract String actionNamePrompt();

    protected abstract String actionNotPerformablePrompt();

    protected abstract List<Goal> goalSelectionOptions();

    protected abstract void onGoalSelected(Goal selectedGoal);

    private Goal askUserForGoalSelection(final String prompt, List<Goal> goals) {
        printLine("What would you like to do?");
        listAllGoals(prompt, goals);
        printLine((goals.size() + 1)  + " -> Close");

        while (true) {
            int selectedGoal = readIntegerFromInput(null);

            if (1 <= selectedGoal && selectedGoal <= goals.size()) {
                return goals.get(selectedGoal - 1);
            }
            if (selectedGoal == goals.size() + 1) {
                return null;
            }

            printLine("Input not among the listed options.");
            printLine("Please try again.");
        }
    }

    private void listAllGoals(final String prompt, List<Goal> goals) {
        for (int i = 0; i < goals.size(); i++) {
            printLine((i + 1)
                    + " -> " + prompt + " '" + goals.get(i).getDescription() + "'");
        }
    }

    private void editGoals() {
        while (true) {
            List<Goal> selectableGoals = goalSelectionOptions();

            if (selectableGoals.isEmpty()) {
                printLine(actionNotPerformablePrompt());
                return;
            }

            Goal selectedGoal = askUserForGoalSelection(actionNamePrompt(), selectableGoals);

            if (selectedGoal == null) {
                return;
            }

            onGoalSelected(selectedGoal);
        }
    }
}

