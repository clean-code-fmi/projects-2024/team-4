package fitness.ui.profile.edit.goals;

import fitness.Goal;
import fitness.Profile;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public final class RemoveGoalsMenu extends AbstractEditGoalsSubmenu {
    private final Profile userProfile;

    public RemoveGoalsMenu(
            final InputStream input,
            final PrintStream output,
            final Profile userProfile) {
        super(input, output);

        this.userProfile = userProfile;
    }

    @Override
    protected String name() {
        return "Remove Goals";
    }

    @Override
    protected String actionNamePrompt() {
        return "Remove goal";
    }

    @Override
    protected String actionNotPerformablePrompt() {
        return "You don't have any goals at the moments";
    }

    @Override
    protected List<Goal> goalSelectionOptions() {
        return new ArrayList<>(userProfile.getGoals());
    }

    @Override
    protected void onGoalSelected(Goal selectedGoal) {
        userProfile.removeGoal(selectedGoal);
        printLine("Goal removed successfully");
    }
}
