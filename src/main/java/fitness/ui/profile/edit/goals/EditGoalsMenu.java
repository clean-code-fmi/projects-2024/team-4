package fitness.ui.profile.edit.goals;

import fitness.ui.AbstractREPLMenuWithSubmenus;
import fitness.ui.Menu;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.function.Supplier;

public final class EditGoalsMenu extends AbstractREPLMenuWithSubmenus {

    private EditGoalsMenu(
            final InputStream input,
            final PrintStream output,
            final List<SubmenuSelectionOption> submenuOptions) {
        super(input, output, submenuOptions);
    }

    public static EditGoalsMenu create(
            final InputStream input,
            final PrintStream output,
            final Supplier<Menu> addGoalsMenuSupplier,
            final Supplier<Menu> removeGoalsMenuSupplier) {
        assert addGoalsMenuSupplier != null;
        assert removeGoalsMenuSupplier != null;


        var submenus = List.of(
                new SubmenuSelectionOption("Add Goals", addGoalsMenuSupplier),
                new SubmenuSelectionOption("Remove Goals", removeGoalsMenuSupplier));

        return new EditGoalsMenu(input, output, submenus);
    }

    @Override
    protected String name() {
        return "Edit Goals";
    }

    @Override
    protected void processMenuSpecificInitFunctionality() {
    }

    @Override
    protected void processMenuSpecificFunctionality() {
    }
}
