package fitness.ui.profile.edit.goals;

import fitness.Goal;
import fitness.Profile;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public final class AddGoalsMenu extends AbstractEditGoalsSubmenu {
    private final Profile userProfile;

    public AddGoalsMenu(
            final InputStream input,
            final PrintStream output,
            final Profile userProfile) {
        super(input, output);

        this.userProfile = userProfile;
    }

    @Override
    protected String name() {
        return "Add Goals";
    }

    @Override
    protected String actionNamePrompt() {
        return "Add goal";
    }

    @Override
    protected String actionNotPerformablePrompt() {
        return "There are no goals available to add at the moment";
    }

    @Override
    protected List<Goal> goalSelectionOptions() {
        ArrayList<Goal> selectableGoals = new ArrayList<>();

        for (Goal goal : Goal.values()) {
            if (!userProfile.getGoals().contains(goal)
                    && !goal.incompatibleWith(userProfile.getGoals())) {
                selectableGoals.add(goal);
            }
        }

        return selectableGoals;
    }

    @Override
    protected void onGoalSelected(Goal selectedGoal) {
        userProfile.addGoal(selectedGoal);
        printLine("Goal added successfully");
    }

}
