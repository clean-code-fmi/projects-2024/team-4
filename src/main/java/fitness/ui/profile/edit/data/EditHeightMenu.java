package fitness.ui.profile.edit.data;

import fitness.Profile;
import fitness.ui.AbstractREPLMenu;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Optional;

public final class EditHeightMenu extends AbstractREPLMenu {

    private final Profile userProfile;

    public EditHeightMenu(
            final InputStream input,
            final PrintStream output,
            final Profile userProfile) {
        super(input, output);

        this.userProfile = userProfile;
    }

    @Override
    protected String name() {
        return "Edit Height";
    }

    @Override
    public void process() {
        super.process();

        String prompt = "Please enter your new height (must be positive)\n"
                + "Current height: " +  userProfile.getHeight() + "\n";

        int height = readIntegerWithinRange(prompt, Optional.of(1), Optional.empty());
        userProfile.setHeight(height);
    }
}
