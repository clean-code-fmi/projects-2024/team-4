package fitness.ui.profile.edit.data;

import fitness.ui.AbstractREPLMenuWithSubmenus;
import fitness.ui.Menu;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.function.Supplier;

public final class EditProfileMenu extends AbstractREPLMenuWithSubmenus {

    private EditProfileMenu(final InputStream input,
                            final PrintStream output,
                            final List<SubmenuSelectionOption> submenuOptions) {
        super(input, output, submenuOptions);
    }

    public static EditProfileMenu create(
            final InputStream input,
            final PrintStream output,
            final Supplier<Menu> editAgeMenuSupplier,
            final Supplier<Menu> editWeightMenuSupplier,
            final Supplier<Menu> editHeightMenuSupplier) {
        assert editAgeMenuSupplier != null;
        assert editWeightMenuSupplier != null;
        assert editHeightMenuSupplier != null;


        var submenus = List.of(
                new SubmenuSelectionOption("Edit Age", editAgeMenuSupplier),
                new SubmenuSelectionOption("Edit Weight", editWeightMenuSupplier),
                new SubmenuSelectionOption("Edit Height", editHeightMenuSupplier));

        return new EditProfileMenu(input, output, submenus);
    }

    @Override
    protected String name() {
        return "Edit (personal) data";
    }

    @Override
    protected void processMenuSpecificInitFunctionality() {
    }

    @Override
    protected void processMenuSpecificFunctionality() {
    }
}
