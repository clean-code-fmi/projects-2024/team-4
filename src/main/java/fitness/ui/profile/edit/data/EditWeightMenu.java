package fitness.ui.profile.edit.data;

import fitness.Profile;
import fitness.ui.AbstractREPLMenu;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Optional;

public final class EditWeightMenu extends AbstractREPLMenu {

    private final Profile userProfile;

    public EditWeightMenu(
            final InputStream input,
            final PrintStream output,
            final Profile userProfile) {
        super(input, output);

        this.userProfile = userProfile;
    }

    @Override
    protected String name() {
        return "Edit Weight";
    }

    @Override
    public void process() {
        super.process();

        String prompt = "Please enter your new weight (must be positive)\n"
                + "Current weight: " + userProfile.getWeight() + "\n";

        int weight = readIntegerWithinRange(prompt, Optional.of(1), Optional.empty());
        userProfile.setWeight(weight);
    }
}
