package fitness.ui.profile.edit.data;

import fitness.Profile;
import fitness.ui.AbstractREPLMenu;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Optional;

public final class EditAgeMenu extends AbstractREPLMenu {

    private final Profile userProfile;

    public EditAgeMenu(
            final InputStream input,
            final PrintStream output,
            final Profile userProfile) {
        super(input, output);

        this.userProfile = userProfile;
    }
    @Override
    protected String name() {
        return "Edit Age";
    }

    @Override
    public void process() {
        super.process();

        String prompt = "Please enter your new age (must be between "
                + Profile.MIN_AGE + " and " + Profile.MAX_AGE + "(inclusive))\n"
                + "Current age: " + userProfile.getAge() + " \n";

        int age = readIntegerWithinRange(prompt,
                Optional.of(Profile.MIN_AGE), Optional.of(Profile.MAX_AGE));
        userProfile.setAge(age);
    }
}
