package fitness.ui;

import fitness.Profile;
import fitness.ui.profile.CreateProfileMenu;
import fitness.ui.food.FoodMenu;
import fitness.ui.profile.ProfileMenu;
import fitness.ui.profile.edit.data.EditAgeMenu;
import fitness.ui.profile.edit.data.EditHeightMenu;
import fitness.ui.profile.edit.data.EditProfileMenu;
import fitness.ui.profile.edit.data.EditWeightMenu;
import fitness.ui.profile.edit.goals.AddGoalsMenu;
import fitness.ui.profile.edit.goals.EditGoalsMenu;
import fitness.ui.profile.edit.goals.RemoveGoalsMenu;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.function.Supplier;

public final class FitnessConsoleApplication {
    private FitnessConsoleApplication() {
    }

    public static void main(String[] args) {
        final InputStream standardInput = System.in;
        final PrintStream standardOutput = System.out;

        CreateProfileMenu createProfileMenu     = new CreateProfileMenu(standardInput, standardOutput);

        createProfileMenu.process();

        Profile userProfile = createProfileMenu.getUserProfile();

        Supplier<Menu> profileMenuSupplier = getProfileMenuSupplier(
                standardInput, standardOutput, userProfile);

        Supplier<Menu> foodMenuSupplier = () ->
            FoodMenu.create(
                standardInput,
                standardOutput);

        MainMenu mainMenu = MainMenu.create(
            standardInput,
            standardOutput,
            profileMenuSupplier,
            foodMenuSupplier,
            () -> null,
            () -> null);

        mainMenu.process();
    }

    private static Supplier<Menu> getProfileMenuSupplier(
            InputStream input, PrintStream output, Profile profile) {

        return () -> ProfileMenu.create(
                input,
                output,
                () -> EditProfileMenu.create(
                        input,
                        output,
                        () -> new EditAgeMenu(
                                input,
                                output,
                                profile),
                        () -> new EditWeightMenu(
                                input,
                                output,
                                profile),
                        () -> new EditHeightMenu(
                                input,
                                output,
                                profile)),
                () -> EditGoalsMenu.create(
                        input,
                        output,
                        () -> new AddGoalsMenu(
                                input,
                                output,
                                profile),
                        () -> new RemoveGoalsMenu(
                                input,
                                output,
                                profile)),
                profile);
    }
}
