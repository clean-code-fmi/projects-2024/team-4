package fitness.ui;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Optional;
import java.util.Scanner;

public abstract class AbstractREPLMenu
        implements Menu {
    private final Scanner input;
    private final PrintStream output;

    protected AbstractREPLMenu(
            final InputStream input, final PrintStream output) {
        assert input != null;
        assert output != null;

        this.input = new Scanner(input);
        this.output = output;
    }

    /**
     * Prints the menu name (see {@link #name()}
     */
    @Override
    public void process() {
        String menuName = name();
        if (menuName != null && !menuName.isEmpty()) {
            output.println(name());
        }
    }

    protected abstract String name();

    protected final String readLineFromInput(final String prompt) {
        if (prompt != null && !prompt.isEmpty()) {
            output.print(prompt);
        }

        return input.nextLine();
    }

    protected final int readIntegerFromInput(final String prompt) {
        return readIntegerWithinRange(prompt, Optional.empty(), Optional.empty());
    }

    protected final int readIntegerWithinRange(final String prompt, final Optional<Integer> lowerBound,
                                               final Optional<Integer> upperBound) {
        while (true) {
            if (prompt != null && !prompt.isEmpty()) {
                output.print(prompt);
            }

            String userInput = input.nextLine();
            try {
                int inputNumber = Integer.parseInt(userInput);

                if (lowerBound.isPresent() && inputNumber < lowerBound.get()
                    || upperBound.isPresent() && upperBound.get() < inputNumber) {
                    printLine("Invalid entry: input out of bounds.");

                    if (lowerBound.isPresent() && upperBound.isPresent()) {
                        printLine("Must be between " + lowerBound.get()
                                + " and " + upperBound.get() + " (inclusive)");
                    } else if (lowerBound.isPresent()) {
                        printLine("Must be at least " + lowerBound.get());
                    } else {
                        printLine("Must be at most " + upperBound.get());
                    }
                } else {
                    return inputNumber;
                }


            } catch (NumberFormatException e) {
                output.println("Please enter a number");
            }
        }
    }

    protected final void printLine(final String line) {
        output.println(line);
    }
}
