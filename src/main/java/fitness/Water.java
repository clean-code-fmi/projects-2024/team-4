package fitness;

public final class Water {

    public enum Portion {
        P_250(250), P_500(500);

        private final int size;

        Portion(int s) {
            size = s;
        }
        public int getSize() {
            return size;
        }

    }

    private int volume;

    public Water(Portion data) {
        setVolume(data);
    }

    public void setVolume(Portion data) {
        if (data == null) {
            throw new IllegalArgumentException("Volume can not be set to null");
        }

        volume = data.getSize();
    }

    public int getVolume() {
        return volume;
    }


}
