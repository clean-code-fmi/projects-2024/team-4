package fitness;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

public final class Profile {
    public static final int MAX_AGE = 100;
    public static final int MIN_AGE = 18;

    private final Set<Goal> goals;
    private int age;
    private int weight; // kilograms
    private int height; // centimeters

    public Profile(int age, int weight, int height) {
        this.goals = EnumSet.noneOf(Goal.class);
        setAge(age);
        setWeight(weight);
        setHeight(height);
    }


    public Profile(int age, int weight, int height, Set<Goal> goals) {
        this.goals = EnumSet.noneOf(Goal.class);
        this.goals.addAll(goals);
        setAge(age);
        setWeight(weight);
        setHeight(height);
    }

    public Set<Goal> getGoals() {
        return Collections.unmodifiableSet(goals);
    }

    public boolean addGoal(Goal goal) {
        return goals.add(goal);
    }

    public boolean removeGoal(Goal goal) {
        return goals.remove(goal);
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age < MIN_AGE || age > MAX_AGE) {
            String message = String.format(
                "Age must be between %d and %d (inclusive)", MIN_AGE, MAX_AGE);
            throw new IllegalArgumentException(message);
        }

        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        if (weight <= 0) {
            throw new IllegalArgumentException("Weight must be positive");
        }

        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        if (height <= 0) {
            throw new IllegalArgumentException("Height must be positive");
        }

        this.height = height;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("Age: ").append(getAge()).append("\n");
        result.append("Weight: ").append(getWeight()).append("\n");
        result.append("Height: ").append(getHeight()).append("\n");

        if (!goals.isEmpty()) {
            result.append("Goals:\n");
        }

        for (Goal goal : getGoals()) {
            result.append("  -> ").append(goal.getDescription()).append("\n");
        }

        return result.toString();
    }
}
