package fitness.food;

import java.time.LocalDate;
import java.util.Scanner;

public final class FoodUIExe {
    private final FoodUI foodUI;

    FoodUIExe() {
        foodUI = new FoodUI();
    }

    public int menuChoice(int menu, Scanner scanner, int choice, int options) {
        do {
            switch (menu) {
                case 0: foodUI.mainMenu(); break;
                case 1: foodUI.subMenu1(); break;
                case 2: foodUI.subMenu2(); break;
                case 3: foodUI.subMenu3(); break;
                case 4: foodUI.subMenu4(); break;
                default: System.out.println("Invalid menu!");
            }
            System.out.println("Please choose an option:");
            choice = foodUI.validateInput(scanner, choice);
        } while (choice < 1 || choice > options);
        System.out.println();
        scanner.nextLine();

        return choice;
    }

    public int detailsMenu(Scanner scanner, int choice) {
        int size = foodUI.getTemplates().getFoodList().size();
        do {
            foodUI.details();
            System.out.println(
                    "Please choose an option:");
            choice = foodUI.validateInput(scanner,
                    choice);
        } while (choice < 1 || choice > size);
        System.out.println();
        scanner.nextLine();

        return choice;
    }

    public void start(Scanner scanner) {
        System.out.println();
        int menu;
        int choice = 0;
        int options;

        while (true) {
            menu = 0;
            options = 3;
            choice = menuChoice(menu, scanner, choice, options);
            if (choice == 1) {
                choice = 0;
                while (choice != 5) {
                    menu = 1;
                    options = 5;
                    choice = menuChoice(menu, scanner, choice, options);
                    if (choice == 1) {
                        choice = 0;
                        foodUI.details();
                    } else if (choice == 2) {
                        choice = 0;
                        Food newFood = foodUI.create(scanner);
                        foodUI.getTemplates().addFood(newFood);
                    } else if (choice == 3) {
                        choice = 0;
                        foodUI.edit(scanner);
                    } else if (choice == 4) {
                        choice = 0;
                        foodUI.delete(scanner);
                    }
                }
                choice = 0;
            } else if (choice == 2) {
                choice = 0;
                LocalDate date;
                date = foodUI.dateInput(scanner);
                while (choice != 5) {
                    menu = 2;
                    options = 5;
                    choice = menuChoice(menu, scanner, choice, options);
                    if (choice == 1) {
                        choice = 0;
                        foodUI.viewLog();
                    } else if (choice == 2) {
                        choice = 0;
                        String mealOfTheDay;
                        while (true) {
                            menu = 3;
                            options = 6;
                            choice =
                                    menuChoice(menu, scanner, choice, options);
                            if (choice == 6) {
                                break;
                            } else {
                                if (choice == 1) {
                                    mealOfTheDay = "Breakfast";
                                } else if (choice == 2) {
                                    mealOfTheDay = "Brunch";
                                } else if (choice == 3) {
                                    mealOfTheDay = "Lunch";
                                } else if (choice == 4) {
                                    mealOfTheDay = "Afternoon snack";
                                } else { //choice == 5
                                    mealOfTheDay = "Dinner";
                                }
                            }
                            choice = 0;
                            menu = 4;
                            options = 3;
                            choice =
                                    menuChoice(menu, scanner, choice, options);
                            if (choice == 1) {
                                if (foodUI.getTemplates()
                                        .getFoodList().isEmpty()) {
                                    System.out.println("No food templates "
                                            + "have been added yet...");
                                    System.out.println();
                                } else {
                                    choice = detailsMenu(scanner, choice);
                                    int index = choice - 1;
                                    foodUI.logFood(date, mealOfTheDay,
                                            foodUI.getTemplates().
                                            getFoodList().get(index));
                                }
                                break;
                            } else if (choice == 2) {
                                Food newFood = foodUI.create(scanner);
                                foodUI.logFood(date, mealOfTheDay, newFood);
                                break;
                            } else {
                                choice = 0;
                            }
                        }
                    } else if (choice == 3) {
                        choice = 0;
                        foodUI.removeFoodFromLog(scanner, date);
                    } else if (choice == 4) {
                        choice = 0;
                        System.out.println("--- Changing date ---");
                        date = foodUI.dateInput(scanner);
                    }
                }
            } else {
                System.out.println("Back to Main Menu...");
                break;
            }
        }
    }
}
