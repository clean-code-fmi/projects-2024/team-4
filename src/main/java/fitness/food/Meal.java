package fitness.food;

import java.util.ArrayList;
import java.util.List;

public final class Meal {
    private String name;
    private List<Food> foodList;

    public Meal(String name) {
        this.name = name;
    }

    public void addFoodToMeal(Food food) {
        if (food != null) {
            this.foodList.add(food);
        }
    }

    public void removeFoodFromMeal(Food food) {
        if (food != null && foodList.contains(food)) {
            this.foodList.remove(food);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Food> getFoodList() {
        return new ArrayList<Food>(foodList);
    }

    public void setFoodList(List<Food> foodList) {
        this.foodList = foodList;
    }

    public int calculateCalories(int calories, int weight) {
        return ((calories * weight) / 100 + (calories * weight) % 100);
    }

    public int getMealCalories() {
        int total = 0;
        for (Food food : foodList) {
            total += calculateCalories(food.getCalories(), food.getWeight());
        }
        return total;
    }
}
