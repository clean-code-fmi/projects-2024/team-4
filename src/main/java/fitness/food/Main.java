package fitness.food;

import java.util.Scanner;

public final class Main {
    private Main() { }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        FoodUIExe foodUIExe = new FoodUIExe();
        foodUIExe.start(scanner);
    }
}
