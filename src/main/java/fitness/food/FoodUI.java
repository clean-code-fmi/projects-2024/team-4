package fitness.food;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public final class FoodUI {
    private final FoodHolder templates = new FoodHolder();
    private final HashMap<LocalDate, FoodHolder> diary = new HashMap<>();

    public FoodHolder getTemplates() {
        return templates;
    }

    public void logFood(LocalDate date, String mealOfTheDay, Food food) {
        diary.computeIfAbsent(date, k -> new FoodHolder()).addFood(food);
    }

    public void removeFoodFromLog(Scanner scanner, LocalDate date) {
        int choice = 0;
        if (diary.containsKey(date)) {
            System.out.println("---------- Log for " + date + " ----------");
            FoodHolder loggedFoods = diary.get(date);
            int size = loggedFoods.getFoodList().size();
            int index;

            System.out.println("(Name, Calories, Weight)");
            loggedFoods.printIndexedFoodList();
            do {
                System.out.println("Which food would you like to remove?");
                choice = validateInput(scanner, choice);
            } while (choice < 1 || choice > size);

            index = choice - 1;
            loggedFoods.removeFood(index);
            if (size == 1) {
                diary.remove(date);
            }
            scanner.nextLine();
        } else {
            System.out.println("Log for " + date + " is empty...");
        }
        System.out.println();
    }

    public void viewLog() {
        if (diary.isEmpty()) {
            System.out.println("Food log is empty...");
            System.out.println();
        } else {
            for (Map.Entry<LocalDate, FoodHolder> entry : diary.entrySet()) {
                LocalDate date = entry.getKey();
                FoodHolder foods = entry.getValue();
                System.out.println("Date: " + date);
                System.out.println("(Name, Calories, Weight)");
                foods.printIndexedFoodList();
                System.out.println("Total calories: " + foods.totalCalories());
                System.out.println();
            }
        }
    }

    public LocalDate dateInput(Scanner scanner) {
        DateTimeFormatter dateFormat =
                DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate date = null;
        String userInput;
        boolean isValid;

        do {
            System.out.println("Enter date (example 21.12.2021):");
            userInput = scanner.nextLine();
            try {
                date = LocalDate.parse(userInput, dateFormat);
                isValid = true;
            } catch (Exception ex) {
                System.out.println("Invalid date format!");
                isValid = false;
            }
        } while (!isValid);
        System.out.println();

        return date;
    }

    public void mainMenu() {
        System.out.println("--- Food Menu ---");
        System.out.println("1) Templates");
        System.out.println("2) Diary");
        System.out.println("3) Back");
    }

    public void subMenu1() {
        System.out.println("--- Templates ---");
        System.out.println("1) Details");
        System.out.println("2) Create");
        System.out.println("3) Edit");
        System.out.println("4) Delete");
        System.out.println("5) Back");
    }

    public void subMenu2() {
        System.out.println("------- Diary -------");
        System.out.println("1) View log");
        System.out.println("2) Log food to diary");
        System.out.println("3) Remove logged food");
        System.out.println("4) Change date");
        System.out.println("5) Back");
    }

    public void subMenu3() {
        System.out.println("---- Food Type ----");
        System.out.println("1) Breakfast");
        System.out.println("2) Brunch");
        System.out.println("3) Lunch");
        System.out.println("4) Afternoon snack");
        System.out.println("5) Dinner");
        System.out.println("6) Back");
    }

    public void subMenu4() {
        System.out.println("------------- Log Option -------------");
        System.out.println("1) Choose food from existing templates");
        System.out.println("2) Freeform");
        System.out.println("3) Back");
    }

    public int validateInput(Scanner scanner, int input) {
        try {
            input = Integer.parseInt(scanner.next());
        } catch (NumberFormatException ex) {
            System.out.println("Input is not an int value!");
        }
        return input;
    }

    public void details() {
        if (templates.getFoodList().isEmpty()) {
            System.out.println("No food templates have been added yet...");
        } else {
            System.out.println(
                    "---------- Available food templates ----------");
            System.out.println(
                    "---------- (Name, Calories, Weight) ----------");
            templates.printIndexedFoodList();
        }
        System.out.println();
    }

    public Food create(Scanner scanner) {
        String name;
        int calories = 0;
        int weight = 0;

        System.out.println("--- Creating new food ---");
        System.out.println("Enter name:");
        name = scanner.nextLine();
        do {
            System.out.println("Enter calories:");
            calories = validateInput(scanner, calories);
        } while (calories <= 0);
        do {
            System.out.println("Enter weight:");
            weight = validateInput(scanner, weight);
        } while (weight <= 0);

        Food newFood = new Food(name, calories, weight);
        System.out.println();
        scanner.nextLine();

        return newFood;
    }

    public void edit(Scanner scanner) {
        if (templates.getFoodList().isEmpty()) {
            System.out.println("There is nothing to edit.");
        } else {
            int size = templates.getFoodList().size();
            int choice = 0;
            int index;

            System.out.println("(Name, Calories, Weight)");
            templates.printIndexedFoodList();

            do {
                System.out.println("Which template would you like to edit?");
                choice = validateInput(scanner, choice);
            } while (choice < 1 || choice > size);
            System.out.println();
            scanner.nextLine();

            index = choice - 1;
            Food food = templates.getFoodList().get(index);

            System.out.println("--- Editing food template for "
                    + food.getName() + " ---");
            String newName;
            int newCalories = 0;
            int newWeight = 0;

            System.out.println("Set new name:");
            newName = scanner.nextLine();
            do {
                System.out.println("Set new calories:");
                newCalories = validateInput(scanner, newCalories);
            } while (newCalories <= 0);
            do {
                System.out.println("Set new weight:");
                newWeight = validateInput(scanner, newWeight);
            } while (newWeight <= 0);

            index = choice - 1;
            (templates.getFoodList().get(index)).setName(newName);
            (templates.getFoodList().get(index)).setCalories(newCalories);
            (templates.getFoodList().get(index)).setWeight(newWeight);

            scanner.nextLine();
        }
        System.out.println();
    }

    public void delete(Scanner scanner) {
        if (templates.getFoodList().isEmpty()) {
            System.out.println("There is nothing to delete.");
        } else {
            int size = templates.getFoodList().size();
            int choice = 0;

            System.out.println("(Name, Calories, Weight)");
            templates.printIndexedFoodList();

            do {
                System.out.println("Which food would you like to delete?");
                choice = validateInput(scanner, choice);
            } while (choice < 1 || choice > size);

            int index = choice - 1;
            templates.removeFood(index);
            scanner.nextLine();
        }
        System.out.println();
    }
}
