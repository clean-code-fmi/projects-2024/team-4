package fitness.food;

public final class Food {

    private String name;
    private int calories;
    private int weight;

    public Food(String name, int calories, int weight) {
        this.setName(name);
        this.setCalories(calories);
        this.setWeight(weight);
    }

    public void setName(String name) {
        if (name != null) {
            this.name = name;
        } else {
            throw new IllegalArgumentException("Name can't be blank!");
        }
    }

    public void setCalories(int calories) {
        if (calories >= 0) {
            this.calories = calories;
        } else {
            throw new IllegalArgumentException("Calories cannot be negative!");
        }
    }

    public void setWeight(int weight) {
        if (weight > 0) {
            this.weight = weight;
        } else {
           throw new IllegalArgumentException("Weight must be positive!");
        }
    }

    public String getName() {
        return this.name;
    }

    public int getCalories() {
        return this.calories;
    }

    public int getWeight() {
        return this.weight;
    }

    public void printFood() {
        System.out.println(getName() + ", "
                + getCalories() + "cal, "
                + getWeight() + "g");
    }
}
