package fitness.food;

import java.util.ArrayList;
import java.util.List;

public final class FoodHolder {
    private List<Food> foodList = new ArrayList<>();

    public List<Food> getFoodList() {
        return foodList;
    }

    public void setFoodList(List<Food> foodList) {
        this.foodList = foodList;
    }

    public void addFood(Food food) {
        this.foodList.add(food);
    }

    public void removeFood(int index) {
        if (this.foodList.get(index) != null) {
            this.foodList.remove(index);
        }
    }

    public void printFoodList() {
        for (Food food : foodList) {
            food.printFood();
        }
    }

    public int totalCalories() {
        int total = 0;
        for (Food food : foodList) {
            total += food.getCalories();
        }
        return total;
    }

    public void printIndexedFoodList() {
        int i = 1;
        for (Food food : foodList) {
            System.out.print(i + ": ");
            food.printFood();
            i++;
        }
    }
}
