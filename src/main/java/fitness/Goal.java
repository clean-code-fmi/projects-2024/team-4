package fitness;

import java.util.Set;

public enum Goal {
    LOSE_WEIGHT("I want to lose weight"),
    GAIN_WEIGHT("I want to gain weight"),
    BUILD_MUSCLE("I want to build muscle mass"),
    GAIN_ENERGY("I want to be more energetic");

    private final String description;

    Goal(String description) {
        this.description = description;
    }

    public final boolean incompatibleWith(Goal other) {
        return this == LOSE_WEIGHT && other == GAIN_WEIGHT
                || this == GAIN_WEIGHT && other == LOSE_WEIGHT;
    }

    public final boolean incompatibleWith(Set<Goal> goalList) {
        for (Goal currentGoal : goalList) {
            if (incompatibleWith(currentGoal)) {
                return true;
            }
        }

        return false;
    }

    public String getDescription() {
        return description;
    }
}
