package fitness.exercise;

import fitness.exercise.pojo.Exercise;

import java.util.ArrayList;
import java.util.List;

public final class Workout {
    private String name;
    private List<Exercise> exerciseList;

    public Workout(String name) {
        this.name = name;
        exerciseList = new ArrayList<>();
    }
    public void addExerciseToWorkout(Exercise exercise) {
        if (exercise != null) {
            this.exerciseList.add(exercise);
        }
    }
    public void removeExerciseFromWorkout(String name) {
        this.exerciseList.remove(searchByName(name));
    }

    public Exercise editExercise(String name, Exercise exercise) {
        if (exercise != null) {
            this.exerciseList.remove(searchByName(name));
            this.exerciseList.add(exercise);
        } else {
            throw new IllegalArgumentException(
                    "Exercise can`t be removed from workout");
        }
        return exercise;
    }
    private int searchByName(String name) {
        for (int i = 0; i < this.exerciseList.size(); i++) {
            if (exerciseList.get(i).getName().equals(name)) {
                return i;
            }
            if (i == this.exerciseList.size()) {
                throw new IllegalArgumentException("Exercise not found");
            }
        }
        return -1;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Exercise> getExerciseList() {
        return new ArrayList<Exercise>(exerciseList);
    }

    public void setExerciseList(List<Exercise> exerciseList) {
        this.exerciseList = exerciseList;
    }

}
