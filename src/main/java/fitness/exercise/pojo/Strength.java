package fitness.exercise.pojo;

public final class Strength extends Exercise {
    private int repsPerSetCount;
    private int setsCount;
    public Strength(String name, String description,
                    int setsCount, int repsPerSetCount) {
        super(name, description, Exercise.STRENGTH);
        this.repsPerSetCount = repsPerSetCount;
        this.setSetsCount(setsCount);
    }
    public int getRepsPerSetCount() {
        return this.repsPerSetCount;
    }
    public int getSetsCount() {
        return this.setsCount;
    }
    public void setRepsPerSetCount(int repsPerSetCount) {
        if (repsPerSetCount <= 0) {
            throw new IllegalArgumentException(
                    "Number of reps must be greater than 0!");
        }
        this.repsPerSetCount = repsPerSetCount;
    }
    public void setSetsCount(int setsCount) {
        if (setsCount <= 0) {
            throw new IllegalArgumentException(
                    "Number of sets must be greater than 0!");
        }
        this.setsCount = setsCount;
    }
    @Override
    public String toString() {
        return super.toString() + "Sets Count: "
                + getSetsCount() + "\n"
                + "Sets Count: "
                + getSetsCount() + "\n";
    }
}
