package fitness.exercise.pojo;

public final class Cardio extends Exercise {
    private int durationInMinutes;
    private int caloriesBurnedPerMinute;
    public Cardio(String name, String description, int caloriesBurnedPerMinute,
                   int durationInMinutes) {
        super(name, description, Exercise.CARDIO);
        this.setDurationInMinutes(durationInMinutes);
        this.setCaloriesBurnedPerMinute(caloriesBurnedPerMinute);
    }
    public void setCaloriesBurnedPerMinute(int caloriesBurnedPerMinute) {
        if (caloriesBurnedPerMinute <= 0) {
            throw new IllegalArgumentException(
                    "Calories burned must be greater than 0!");
        }
        this.caloriesBurnedPerMinute = caloriesBurnedPerMinute;
    }
    public void setDurationInMinutes(int durationInMinutes) {
        if (durationInMinutes <= 0) {
            throw new IllegalArgumentException(
                    "Exercise duration must be greater than 0!");
        }
        this.durationInMinutes = durationInMinutes;
    }
    public int getCaloriesBurnedPerMinute() {
        return this.caloriesBurnedPerMinute;
    }
    public int getDurationInMinutes() {
        return this.durationInMinutes;
    }
    public int calculateTotalCaloriesBurned() {
        return this.getCaloriesBurnedPerMinute() * this.getDurationInMinutes();
    }

    @Override
    public String toString() {
        return super.toString()
                + "Calories burned per minute: "
                + getCaloriesBurnedPerMinute() + " cal.\n"
                + "Exercise duration: "
                + getDurationInMinutes() + " minutes\n"
                + "Total calories burned: "
                + calculateTotalCaloriesBurned() + " cal.\n";
    }
}
