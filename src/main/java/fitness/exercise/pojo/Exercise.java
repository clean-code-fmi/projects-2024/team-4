package fitness.exercise.pojo;

public abstract class Exercise {
    private String name;
    private String description;
    private final String type;
    public static final String CARDIO = "Cardio";
    public static final String STRENGTH = "Strength";

    public Exercise(String name, String description, String type) {
        this.setName(name);
        this.setDescription(description);
        this.type = type;
    }

    /**
     *  @param name
     */
    public void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException(
                    "Exercise name can't be null!");
        }
        this.name = name;
    }

    /**
     * @param description
     */
    public void setDescription(String description) {
        if (description == null) {
            throw new IllegalArgumentException(
                    "Exercise description can't be null!");
        }
        this.description = description;
    }
    /**
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * @return type
     */
    public String getType() {
        return this.type;
    }
    /**
     * @return override toString method
     */
    @Override
    public String toString() {
        return "Name: "
                + getName() + "\n"
                + "Description: "
                + getDescription() + "\n";
    }
}
