package fitness.exercise;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public final class WorkoutDiaryHolder {
    private List<WorkoutDiary> workoutDiaryList;

    public List<WorkoutDiary> getWorkoutDiaryList() {
        return new ArrayList<WorkoutDiary>(workoutDiaryList);
    }
    public WorkoutDiaryHolder() {
        this.workoutDiaryList = new ArrayList<>();
    }
    public void add(WorkoutDiary workoutDiary) {
        workoutDiaryList.add(workoutDiary);
    }
    public WorkoutDiary edit(WorkoutDiary workoutDiary) {

        return workoutDiary;
    }
    public void delete(WorkoutDiary workoutDiary) {

    }
    public WorkoutDiary searchByDate(LocalDate date) {
        if (this.workoutDiaryList != null) {
            for (WorkoutDiary workoutDiary : workoutDiaryList) {
                if (workoutDiary.getDate().equals(date)) {
                    return workoutDiary;
                }
            }
        }
        WorkoutDiary workoutDiary = new WorkoutDiary(date);
        this.workoutDiaryList.add(workoutDiary);
        return workoutDiary;
    }

}
