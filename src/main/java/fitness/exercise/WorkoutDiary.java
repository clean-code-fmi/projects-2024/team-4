package fitness.exercise;

import java.time.LocalDate;
import fitness.exercise.pojo.Exercise;

public final class WorkoutDiary {
    private LocalDate date;
    private Workout workout;

    public WorkoutDiary(LocalDate date) {
        this.date = date;
        this.workout = new Workout(date.toString());
    }

    public void addExercise(Exercise exercise) {
        workout.addExerciseToWorkout(exercise);
    }
    public Exercise editExercise(String name, Exercise exercise) {
        return workout.editExercise(name, exercise);
    }
    public void deleteExercise(String name) {
        this.workout.removeExerciseFromWorkout(name);
    }
    public Workout getWorkout() {
      return this.workout;
    }

    public LocalDate getDate() {
        return date;
    }
}
