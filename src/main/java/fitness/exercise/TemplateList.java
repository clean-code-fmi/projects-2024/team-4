package fitness.exercise;
import fitness.exercise.pojo.Exercise;
import java.util.ArrayList;
import java.util.List;

/**
 * This class collects all exercises in app
 */
public final class TemplateList {
    private final List<Exercise> exerciseList;
    public TemplateList() {
        exerciseList = new ArrayList<>();
    }
    public void addExercise(Exercise data) {
        exerciseList.add(data);
    }
    public List<Exercise> getExerciseList() {
        return new ArrayList<>(exerciseList);
    }
    public void deleteExercise(String name) {
        try {
            this.exerciseList.remove(searchByName(name));
        } catch (IllegalArgumentException i) {
            System.out.println("Not found exercise to be deleted");
        }

    }
    public Exercise edit(String oldName, Exercise newExercise) {
        deleteExercise(oldName);
        addExercise(newExercise);
        return newExercise;
    }
    private int searchByName(String name) {
        for (int i = 0; i < this.exerciseList.size(); i++) {
            if (exerciseList.get(i).getName().equals(name)) {
                return i;
            }
        }
        return -1;
    }
}
