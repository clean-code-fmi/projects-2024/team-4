package fitness.exercise.ui;

import fitness.exercise.TemplateList;
import fitness.exercise.WorkoutDiary;
import fitness.exercise.WorkoutDiaryHolder;
import fitness.exercise.pojo.Cardio;
import fitness.exercise.pojo.Exercise;
import fitness.exercise.pojo.Strength;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

public final class ExerciseUI {
    private TemplateList exerciseList = new TemplateList();
    private WorkoutDiaryHolder workoutDiaryHolder = new WorkoutDiaryHolder();
    private String date = "///";
    private DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("d/MM/yyyy");

    public void start() {
        Scanner scanner;
        boolean isBackState = false;
        String choice;
            scanner = new Scanner(System.in);
            System.out.println("1.)Templates" + "\n" + "2.)Diary"
                    + "\n" + "3.)Back" + "\n" + "Your choice: ");
            choice = scanner.nextLine();
            if (choice.equals("1")) {
                System.out.println("1.)Create" + "\n" + "2.)Edit" + "\n"
                        + "3.)Delete" + "\n" + "4.)Back"
                        + "\n" + "Your choice: ");
                choice += "." + scanner.nextLine();
            }
            if (choice.equals("1.1") || choice.equals("1.2")
                    || choice.equals("1.3")) {
                System.out.println("1.)Cardio"
                        + "\n" + "2.)Strength"
                        + "\n" + "Your choice: ");
                choice += "." + scanner.nextInt();
            }
            if (choice.equals("2")) {
                System.out.println("Write the date of the diary "
                        + "\n" + "d/mm/yyyy: ");
                date = new Scanner(System.in).nextLine();
                LocalDate localDate = LocalDate.parse(date, formatter);
                List<Exercise> workoutExercises;
                if (workoutDiaryHolder.searchByDate(localDate)
                        .getWorkout().getExerciseList() != null) {
                    workoutExercises =  workoutDiaryHolder
                            .searchByDate(localDate)
                            .getWorkout()
                            .getExerciseList();
                    for (Exercise exercise : workoutExercises) {
                        System.out.println(exercise.toString());
                    }
                }
                System.out.println("1.)Log exercise to diary " + "\n"
                        + "2.)Remove logged exercise from diary" + "\n"
                        + "4.)Back" + "\n"
                        + "Your choice: ");
                choice += "." + scanner.nextInt();
                if (choice.equals("2.1") || choice.equals("2.2")) {
                    System.out.println("1.)Cardio"
                            + "\n" + "2.)Strength"
                            + "\n" + "Your choice: ");
                    choice += "." + scanner.nextInt();
                }
            }
            if (choice.equals("2.1.1")) {
                LocalDate localDate = LocalDate.parse(date, formatter);
                System.out.println("1.)From Templates"
                        + "\n" + "2.)Free form"
                        + "\n" + "Your choice: ");
                choice += "." + scanner.nextInt();
                if (choice.equals("2.1.1.1")) {
                    printCardioExercises();
                    System.out.println("Please write the name of template"
                            + " which you want to add to the diary");
                    scanner = new Scanner(System.in);
                    String name = scanner.nextLine();
                    fillDiaryFromTemplate(name, localDate);
                }
                if (choice.equals("2.1.2.1")) {
                    printStrengthExercises();
                    System.out.println("Please write the name of template"
                            + " which you want to add to the diary");
                    scanner = new Scanner(System.in);
                    String name = scanner.nextLine();
                    fillDiaryFromTemplate(name, localDate);
                }
                if (choice.equals("2.1.1.2")) {
                    workoutDiaryHolder.searchByDate(localDate).getWorkout()
                            .addExerciseToWorkout(inputCardio());
                }
                if (choice.equals("2.1.2.2")) {
                    workoutDiaryHolder.searchByDate(localDate).getWorkout()
                            .addExerciseToWorkout(inputStrength());
                }

            }
            if (choice.equals("2.1.2")) {
                LocalDate localDate = LocalDate.parse(date, formatter);
                workoutDiaryHolder.searchByDate(localDate).getWorkout()
                        .addExerciseToWorkout(inputStrength());
            }
            if (choice.equals("2.2.1")) {
                LocalDate localDate = LocalDate.parse(date, formatter);
                removeDiaryExercise(Exercise.CARDIO, localDate);
            }
            if (choice.equals("2.2.2")) {
                LocalDate localDate = LocalDate.parse(date, formatter);
                removeDiaryExercise(Exercise.STRENGTH, localDate);
            }

            if (choice.equals("1.1.1")) {
                exerciseList.addExercise(inputCardio());
            }
            if (choice.equals("1.1.2")) {
                exerciseList.addExercise(inputStrength());
            }
            if (choice.equals("1.2.1")) {
                printCardioExercises();
                String oldName = "Example";
                System.out.println("Which exercise you"
                        + " want to change, please write old name: ");
                oldName = new Scanner(System.in).nextLine();
                this.exerciseList.edit(oldName, inputCardio());
            }
            if (choice.equals("1.2.2")) {
                printStrengthExercises();
                String oldName = "Example";
                System.out.println("Which exercise you"
                        + " want to change, please write old name: ");
                oldName = new Scanner(System.in).nextLine();
                this.exerciseList.edit(oldName, inputStrength());
            }
            if (choice.equals("1.3.1")) {
                printCardioExercises();
                deleteExercise(Exercise.CARDIO);
            }
            if (choice.equals("1.3.2")) {
                printStrengthExercises();
                deleteExercise(Exercise.STRENGTH);
            }
            if (choice.equals("1.4")) {
                isBackState = true;
            }
            if (choice.equals("3")) {
                isBackState = true;
                return;
            } else {
                start();
            }
    }

    private Exercise inputStrength() {
        boolean isValidInput = false;
        Strength strength = new Strength("", "", 1, 1);
        System.out.println("Write name +'double space', description of the "
                + "exercise + 'double space' + replies in set"
                + " +'double space' and number of sets + 'double space'"
                + "\n");
        while (!isValidInput) {
            Scanner scanner = new Scanner(System.in);
            String line = scanner.nextLine();
            String[] exercise = line.split("  ");
            try {
                strength = new Strength(exercise[0], exercise[1],
                        Integer.valueOf(exercise[2]).intValue(),
                        Integer.valueOf(exercise[3]).intValue());
                isValidInput = true;
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException("Wrong input!", e);
            }
        }
        return strength;
    }

    private Exercise inputCardio() {
        Cardio cardio = null;
        boolean isValidInput = false;
        System.out.println("Write name +'double space', description of the "
                + "exercise + 'double space' + calories that "
                + "burns exercise per"
                + "minute +'double space' and duration in minutes "
                + "+ 'double space'"
                + "\n");
        while (!isValidInput) {
            Scanner scanner = new Scanner(System.in);
            String line = scanner.nextLine();
            String[] exercise = line.split("  ");
            try {
                cardio = new Cardio(exercise[0], exercise[1],
                        Integer.valueOf(exercise[2]).intValue(),
                        Integer.valueOf(exercise[3]).intValue());
                isValidInput = true;
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException("Wrong input!", e);
            }
        }
        return cardio;
    }
    private void printCardioExercises() {
        for (Exercise exercise : exerciseList.getExerciseList()) {
            if (exercise.getType().equals(Exercise.CARDIO)) {
                System.out.println(((Cardio) exercise)
                        .toString() + "\n");
            }
        }
    }
    private void printStrengthExercises() {
        for (Exercise exercise : exerciseList.getExerciseList()) {
            if (exercise.getType().equals(Exercise.STRENGTH)) {
                System.out.println(((Strength) exercise)
                        .toString() + "\n");
            }
        }
    }
    private void deleteExercise(String type) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write name of the" + type + " exercise that"
                + " you want to delete and press 'double space'" + "\n");
        String name = scanner.nextLine();
        this.exerciseList.deleteExercise(name);
    }
    private void removeDiaryExercise(String type, LocalDate localDate) {

        for (WorkoutDiary workoutDiary : workoutDiaryHolder
                .getWorkoutDiaryList()) {
            for (Exercise exercise : workoutDiary.getWorkout()
                    .getExerciseList()) {
                exercise.toString();
            }
        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write name of the" + type + " exercise that"
                + " you want to delete and press 'double space'" + "\n");
        String name = scanner.nextLine();
        for (WorkoutDiary workoutDiary : workoutDiaryHolder
                .getWorkoutDiaryList()) {
            for (Exercise exercise : workoutDiary.getWorkout()
                    .getExerciseList()) {
                if (exercise.getName().equals(name)) {
                    workoutDiary.getWorkout()
                            .getExerciseList().remove(exercise);
                }
            }
        }
    }
    private void fillDiaryFromTemplate(String name, LocalDate localDate) {
        for (Exercise exercise : this.exerciseList
                .getExerciseList()) {
            if (exercise.getName().equals(name)) {
                workoutDiaryHolder.searchByDate(localDate)
                        .getWorkout()
                        .addExerciseToWorkout(exercise);
            }
        }
    }
}

