package fitness;

import java.util.ArrayList;
import java.util.List;

public final class WaterContainer {
    private final List<Water> waterList;

    WaterContainer() {
        waterList = new ArrayList<Water>();
    }

    public void add(Water data) {
        waterList.add(data);
    }

    public List<Water> getWaterList() {
        return new ArrayList<Water>(waterList);
    }


}
