package fitness;

import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.EnumSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ProfileTest {
    @Test
    public void whenCreatingProfileExpectCorrectData() {
        var goals = EnumSet.of(Goal.GAIN_WEIGHT, Goal.BUILD_MUSCLE);

        var profile = new Profile(20, 100, 180, goals);

        assertEquals(20, profile.getAge());
        assertEquals(100, profile.getWeight());
        assertEquals(180, profile.getHeight());
        assertEquals(goals, profile.getGoals());
    }

    @Test
    void whenCreatingProfileAgeTooLowExpectException() {
        assertThrows(IllegalArgumentException.class,
                () -> new Profile(10, 80, 150, Collections.emptySet()));
    }

    @Test
    void whenCreatingProfileAgeTooHighExpectException() {
        assertThrows(IllegalArgumentException.class,
                () -> new Profile(105, 80, 150, Collections.emptySet()));
    }

    @Test
    public void whenCreatingProfileNegativeWeightExpectException() {
        assertThrows(IllegalArgumentException.class,
            () -> new Profile(20, -10, 150, Collections.emptySet()));
    }

    @Test
    public void whenCreatingProfileNegativeHeightExpectException() {
        assertThrows(IllegalArgumentException.class,
            () -> new Profile(20, 100, -10, Collections.emptySet()));
    }

    @Test
    void whenSettingAgeExpectCorrectAgeSet() {
        var profile = new Profile(30, 60, 150, EnumSet.noneOf(Goal.class));

        profile.setAge(25);

        assertEquals(25, profile.getAge());
    }

    @Test
    void whenSettingAgeTooLowExpectException() {
        var profile = new Profile(25, 60, 150, EnumSet.noneOf(Goal.class));

        assertThrows(IllegalArgumentException.class,
            () -> profile.setAge(5));
    }

    @Test
    void whenSettingAgeTooHighExpectException() {
        var profile = new Profile(25, 60, 150, EnumSet.noneOf(Goal.class));

        assertThrows(IllegalArgumentException.class,
            () -> profile.setAge(200));
    }

    @Test
    public void whenSettingHeightExpectCorrectHeightSet() {
        var profile = new Profile(20, 100, 100, EnumSet.noneOf(Goal.class));

        profile.setHeight(180);

        assertEquals(180, profile.getHeight());
    }

    @Test
    public void whenSettingHeightNegativeExpectException() {
        var profile = new Profile(20, 100, 100, EnumSet.noneOf(Goal.class));

        assertThrows(IllegalArgumentException.class,
            () -> profile.setHeight(-20));
    }

    @Test
    public void whenSettingWeightExpectCorrectWeightSet() {
        var profile = new Profile(20, 100, 100, EnumSet.noneOf(Goal.class));

        profile.setWeight(90);

        assertEquals(90, profile.getWeight());
    }

    @Test
    public void whenSettingWeightNegativeExpectException() {
        var profile = new Profile(20, 100, 100, EnumSet.noneOf(Goal.class));

        assertThrows(IllegalArgumentException.class,
            () -> profile.setWeight(-50));
    }

    @Test
    public void whenAddingGoalExpectGoalAdded() {
        var profile = new Profile(20, 100, 100, EnumSet.of(Goal.GAIN_WEIGHT));

        boolean added = profile.addGoal(Goal.GAIN_ENERGY);

        assertTrue(added);
        assertTrue(profile.getGoals().contains(Goal.GAIN_ENERGY));
        assertEquals(2, profile.getGoals().size());
    }

    @Test
    public void whenAddingExistingGoalExpectGoalNotAdded() {
        var profile = new Profile(20, 100, 100, EnumSet.of(Goal.GAIN_WEIGHT));

        boolean added = profile.addGoal(Goal.GAIN_WEIGHT);

        assertFalse(added);
        assertTrue(profile.getGoals().contains(Goal.GAIN_WEIGHT));
        assertEquals(1, profile.getGoals().size());
    }

    @Test
    public void whenRemovingGoalExpectGoalRemoved() {
        var profile = new Profile(
            20, 100, 100, EnumSet.of(Goal.GAIN_WEIGHT, Goal.GAIN_ENERGY));

        boolean removed = profile.removeGoal(Goal.GAIN_ENERGY);

        assertTrue(removed);
        assertFalse(profile.getGoals().contains(Goal.GAIN_ENERGY));
        assertEquals(1, profile.getGoals().size());
    }

    @Test
    public void whenRemovingNonExistingGoalExpectGoalNotRemoved() {
        var profile = new Profile(
            20, 100, 100, EnumSet.of(Goal.GAIN_WEIGHT, Goal.GAIN_ENERGY));

        boolean removed = profile.removeGoal(Goal.BUILD_MUSCLE);

        assertFalse(removed);
        assertFalse(profile.getGoals().contains(Goal.BUILD_MUSCLE));
        assertEquals(2, profile.getGoals().size());
    }
}
