package fitness;

import fitness.food.Food;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public final class FoodTest {

    @Test
    void foodConstructorTest() {
        final Food food = new Food("Banana", 89, 100);
        assertEquals("Banana", food.getName());
        assertEquals(89, food.getCalories());
        assertEquals(100, food.getWeight());
    }

    @Test
    void foodSettersTest() {
        final Food food = new Food("Banana", 89, 100);
        food.setName("Apple");
        food.setCalories(52);
        food.setWeight(100);
        assertEquals("Apple", food.getName());
        assertEquals(52, food.getCalories());
        assertEquals(100, food.getWeight());
    }


}
