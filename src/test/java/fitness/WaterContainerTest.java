package fitness;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public final class WaterContainerTest {

    @Test
    void whenAddingWaterLogExpectALogEntryAdded() {
        final Water water1 = new Water(Water.Portion.P_250);
        final Water water2 = new Water(Water.Portion.P_500);
        final WaterContainer waterContainer = new WaterContainer();

        waterContainer.add(water1);
        waterContainer.add(water2);

        assertEquals(250, waterContainer.getWaterList().get(0).getVolume());
        assertEquals(500, waterContainer.getWaterList().get(1).getVolume());

    }
}
