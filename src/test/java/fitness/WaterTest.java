package fitness;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public final class WaterTest {

    @Test
    void waterConstructorWorksProperly() {
        final Water water = new Water(Water.Portion.P_250);
        assertEquals(250, water.getVolume());
    }

    @Test
    void waterSetVolumeWorksProperly() {
        final Water water = new Water(Water.Portion.P_250);
        water.setVolume(Water.Portion.P_500);
        assertEquals(500, water.getVolume());
    }

}
